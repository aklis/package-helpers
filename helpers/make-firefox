#!/bin/bash
#
#    Copyright (C) 2008-2015  Ruben Rodriguez <ruben@trisquel.info>
#    Copyright (C) 2015       Santiago Rodriguez <santi@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=44

. ./config

rm debian/control

# revert https://bug851702.bugzilla.mozilla.org/attachment.cgi?id=733785
patch -p1 < $DATA/enable-js-options.patch
cp ./browser/components/preferences/advanced-scripts.xul ./browser/components/preferences/in-content/

# js settings
cat $DATA/settings.js >> debian/vendor-firefox.js

# Replace ubufox recommendation
sed 's/xul-ext-ubufox/xul-ext-youtube-html5-video-player/' -i debian/control.in

sed '/mozilla.org\/legal/d' -i services/healthreport/healthreport-prefs.js
cat << EOF >>services/healthreport/healthreport-prefs.js
pref("datareporting.healthreport.infoURL", "https://trisquel.info/legal");
EOF

sed 's%https://www.mozilla.org/legal/privacy/%https://trisquel.info/legal%' -i ./modules/libpref/init/all.js ./browser/app/profile/firefox.js ./toolkit/content/aboutRights.xhtml

#sed 's%https://www.mozilla.org/firefox/central/%https://trisquel.info/browser%' -i browser/base/content/browser-appmenu.inc

# Remove Google API key
rm debian/ga debian/go
sed '/Google API/,$ d' debian/config/mozconfig.in -i

# Enable gst support
[ $REVISION = "6.0" ] && apt-get install -y --force-yes libgstreamermm-0.10-dev
[ $REVISION = "6.0" ] && echo "ac_add_options --enable-gstreamer=0.10" >> debian/config/mozconfig.in
[ $REVISION = "7.0" ] && echo "ac_add_options --enable-gstreamer=1.0" >> debian/config/mozconfig.in
sed 's/1204/60/g; s/1210/65/g; s/1404/70/g' -i debian/config/mozconfig.in debian/firefox-dev.install.in debian/firefox-dev.links.in

sed 's/com.ubuntu/org.trisquel/' -i debian/config/mozconfig.in

# Disable DRM support
echo ac_add_options --disable-eme >> debian/config/mozconfig.in
sed '/gmp-clearkey/d' -i ./debian/firefox.install.in

# Locale packages should provide firefox-locale-$LANG
sed "s/Provides.*/Provides: firefox-locale-@LANGCODE@/" -i debian/control.langpacks

# Remove Ubuntu bookmarks
sed -i /ubuntu-bookmarks/d debian/patches/series
rm debian/patches/ubuntu-bookmarks*

# Remove Ubuntu l10n search preferences
sed -i /ubuntu-search-defaults/d debian/patches/series
rm debian/patches/ubuntu-search-defaults*

#Unbrand url codes for google and amazon
find debian/searchplugins |grep google| xargs -i /bin/sed '/ubuntu/d; /channel/d' -i {}
find debian/searchplugins |grep amazon| xargs -i /bin/sed '/canoniccom/d;' -i {}

#Replace canonical referer with our own for duckduckgo
find debian/searchplugins |grep duck| xargs -i /bin/sed 's/canonical/trisquel/' -i {}
replace "mozilla.com/plugincheck" "trisquel.info/browser" .

# contact link
#sed 's_https://input.mozilla.org/feedback_https://trisquel.info/contact_' -i browser/base/content/utilityOverlay.js

cat << EOF > debian/distribution.ini
[Global]
id=trisquel
version=$REVISION
about=Abrowser for Trisquel GNU/Linux

[Preferences]
app.distributor = "trisquel"
app.distributor.channel = "trisquel"
app.partner.ubuntu = "trisquel"
EOF

sed  "s/^MOZ_APP_NAME\t.*/MOZ_APP_NAME\t\t:= abrowser/;" debian/build/config.mk -i
sed  "s/^MOZ_PKG_NAME\t.*/MOZ_PKG_NAME\t\t:= abrowser/;" debian/build/config.mk -i

############################################################################3
############################################################################3
############################################################################3
sed "s_^Maintainer.*_Maintainer: $DEBFULLNAME <$DEBEMAIL>_g" -i debian/control.in

# Replace Firefox branding
find -type d | grep firefox | xargs rename s/firefox/abrowser/
find -type f | grep firefox | xargs rename s/firefox/abrowser/
find -type f | grep Firefox | xargs rename s/Firefox/Abrowser/
replace(){
find $3 -type f |grep -v changelog |grep -v copyright | xargs -i sed -i s^"$1"^"$2"^g "{}"
}
replace "Mozilla Firefox" "Abrowser" .
replace firefox abrowser .
replace Firefox Abrowser .
replace FIREFOX ABROWSER .
replace " Mozilla " " Trisquel " .
sed -i '2s/^Source:.*/Source: firefox/' debian/control.in
replace PACKAGES/abrowser PACKAGES/firefox .
sed s/Trisquel/Mozilla/ l10n/compare-locales/scripts/compare-locales -i
sed s/Trisquel/Mozilla/ l10n/compare-locales/setup.py -i
replace "iceweasel, abrowser" "iceweasel, firefox" .
replace "Replaces: abrowser" "Replaces: firefox" .
#sed s/Ubuntu/Trisquel/g debian/rules -i
sed s/ubuntu/trisquel/g debian/distribution.ini -i
sed 's/ubuntu_version/trisquel_version/; s/Ubuntu 10.10/Trisquel 4.0/; s/1010/40/' -i debian/abrowser.postinst.in
replace "Adobe Flash" "Flash" .

# abrowser should provide firefox
sed '/Package: @MOZ_PKG_NAME@-dev/,/Description:/ s/Provides:/Provides: firefox-dev, /' debian/control.in -i
sed '/Package: @MOZ_PKG_NAME@$/,/Description:/ s/Provides:/Provides: firefox, /' debian/control.in -i

# Branding files
rm browser/branding/* -rf
cp -a $DATA/branding/ browser/branding/official
cat << EOF > debian/config/branch.mk
CHANNEL                 = release
MOZ_WANT_UNIT_TESTS     = 0
# MOZ_BUILD_OFFICIAL    = 1
MOZ_ENABLE_BREAKPAD     = 0

MOZILLA_REPO = http://hg.mozilla.org/releases/mozilla-release
L10N_REPO = http://hg.mozilla.org/releases/l10n/mozilla-release
EOF

# Replace about:home
rm browser/base/content/abouthome -rf
cp $DATA/abouthome -a browser/base/content
sed '/mozilla.*png/d' -i ./browser/base/jar.mn

# Delete stuff we don't use and that may contain trademaked logos
rm -rf ./browser/metro ./addon-sdk/source/doc/static-files/media ./browser/themes/windows ./browser/themes/osx ./b2g

#Trisquel custom bookmarks
cp $DATA/bookmarks.html.in browser/locales/generic/profile/bookmarks.html.in

#Trisquel custom search engines
cp $DATA/searchplugins/*.xml debian/searchplugins/en-US/
cp $DATA/searchplugins/searchplugins.conf debian/config/

# install aboutabrowser extension
cp $DATA/aboutabrowser@trisquel.info -r debian
echo "debian/aboutabrowser@trisquel.info @MOZ_ADDONDIR@/extensions/" >> debian/abrowser.install.in

# Disable newtab "What is this" popup and config button
cat << EOF >> browser/themes/linux/newtab/newTab.css
#newtab-customize-button, #newtab-intro-what{
display:none
}
EOF

# Disable search field at extensions panel
#sed  '/header-search/d; /search.placeholder/d' -i toolkit/mozapps/extensions/content/extensions.xul
cat << EOF >> toolkit/mozapps/extensions/content/extensions.css
#header-search {
  display:none;
}
EOF

find -wholename '*/brand.dtd' |xargs /bin/sed 's/trademarkInfo.part1.*/trademarkInfo.part1 "">/' -i

for STRING in community.end3 community.exp.end community.start2 community.mozillaLink community.middle2 community.creditsLink community.end2 contribute.start contribute.getInvolvedLink contribute.end channel.description.start channel.description.end
do
 find -name aboutDialog.dtd | xargs sed -i "s/ENTITY $STRING.*/ENTITY $STRING \"\">/"
done

for STRING in rights.intro-point3-unbranded rights.intro-point4a-unbranded rights.intro-point4b-unbranded rights.intro-point4c-unbranded
do
 find -name aboutRights.dtd | xargs sed -i "s/ENTITY $STRING.*/ENTITY $STRING \"\">/"
done

replace www.mozilla.com/abrowser/central trisquel.info/browser
replace www.mozilla.com/legal/privacy trisquel.info/legal

sed -i 's/<a\ href\=\"http\:\/\/www.mozilla.org\/\">Mozilla\ Project<\/a>/<a\ href\=\"http\:\/\/www.trisquel.info\/\"\>Trisquel\ Project<\/a>/g' browser/base/content/overrides/app-license.html

# We went too far...
replace "Trisquel Public" "Mozilla Public" .
replace "Trisquel Foundation" "Mozilla Foundation" .
replace "Trisquel Corporation" "Mozilla Corporation" .
replace "abrowser.com" "firefox.com" .
#sed -i 's/iceweasel, abrowser, icecat,/iceweasel, firefox, icecat,/g' debian/control.in
sed '/Provides/s/abrowser-locale/firefox-locale/' -i debian/control.langpacks

# Restore useragent to Firefox
sed '/MOZILLA_UAVERSION/ s:Abrowser/:Firefox/:' -i netwerk/protocol/http/nsHttpHandler.cpp

# Set migrator scripts
sed 's/Abrowser/Firefox/g; s/abrowser/firefox/g' -i browser/components/migration/AbrowserProfileMigrator.js

# Postinst script to manage profile migration and system links
echo '

if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] ; then

[ -f /usr/bin/firefox ] || ln -s /usr/bin/abrowser /usr/bin/firefox

for HOMEDIR in $(grep :/home/ /etc/passwd |grep -v usbmux |grep -v syslog|cut -d : -f 6)
do
    [ -d $HOMEDIR/.mozilla/abrowser ] && continue || true
    [ -d $HOMEDIR/.mozilla/firefox ] || continue
    echo Linking $HOMEDIR/.mozilla/firefox into $HOMEDIR/.mozilla/abrowser
    ln -s $HOMEDIR/.mozilla/firefox $HOMEDIR/.mozilla/abrowser
done 
fi
exit 0 ' >> debian/abrowser.postinst.in

debian/rules debian/control
touch -d "yesterday" debian/control
debian/rules debian/control

# Regenerate addon-sdk/moz.build
./mach generate-addon-sdk-moz-build

#https://bugzilla.mozilla.org/show_bug.cgi?id=1047803
sed 's/--enable-optimize$/--enable-optimize="-O2"/' -i debian/config/mozconfig.in

changelog  "Rebranded for Trisquel"

compile
